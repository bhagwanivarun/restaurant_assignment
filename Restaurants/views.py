# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import View
from .models import Restaurant, Menu, Discount
import json

# Create your views here.

def get_hotels_by_city(request, input_city):
	
	try:
		hotels = Restaurant.objects.filter(city=input_city)
		output = '\n'.join([p.restaurant_name for p in hotels.all()])
	except:
		return HttpResponse("No restaurants for given city or invalid city name")
	return HttpResponse(output)

def get_hotel_info(request, input_hotel):

	try:
		hotel = Restaurant.objects.get(restaurant_name=input_hotel)
	except:
		return HttpResponse("No such restaurant in our system")
	return HttpResponse("name =%s,lattitude=%f,longitude=%f,city=%s,discount code=%s, address=%s,type=%s"%((hotel.restaurant_name),(hotel.lattitude),(hotel.longitude),(hotel.city),(hotel.discount_code),(hotel.address),(hotel.type)))

def get_dishes_by_hotel(request, input_hotel):

	try:
		hotel = Restaurant.objects.get(restaurant_name=input_hotel)
		dishes = hotel.menu_set.all()
	except:
		return HttpResponse("No such restaurant in our system")
	output_dishes = ', '.join([q.dish_name for q in dishes])
	return HttpResponse(output_dishes)



def order(request, input_dish):

	try:
		dish = Menu.objects.get(dish_name=input_dish)		
	except:
		return HttpResponse("No such dish on the menu")
	amount = float(dish.price)
	return HttpResponse("Your order for %s is confirmed.You will be charged amount %f"%((dish.dish_name),(amount)))

def discounted_order(request, input_dish, input_code):

	dish = Menu.objects.get(dish_name=input_dish)
	try:
		offer = Discount.objects.get(discount_code=input_code)
	except:
		amount = float(dish.price)
		return HttpResponse("Your discount code didn't work.Your order for %s is confirmed.You will be charged amount %f"%((dish.dish_name),(amount)))
	amount = float(float(dish.price)*(1-float(offer.discount_percentage)/100))
	return HttpResponse("Your discount code worked.Your order for %s is confirmed.You will be charged amount %f"%((dish.dish_name),(amount)))


