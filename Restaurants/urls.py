from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from . import views

urlpatterns = [
    url((r'^(?P<input_city>\w+)$'), views.get_hotels_by_city, name='get_hotels_by_city'),
    url((r'^info/(?P<input_hotel>\w+)$'), views.get_hotel_info, name='get_hotel_info'),
    url((r'^dishes/(?P<input_hotel>\w+)$'), views.get_dishes_by_hotel, name='get_dishes_by_hotel'),
    url((r'^order/(?P<input_dish>\w+)/$'), views.order, name='order'),
    url((r'^discounted_order/(?P<input_dish>\w+)/(?P<input_code>\w+)$'), views.discounted_order, name='discounted_order'),
]
