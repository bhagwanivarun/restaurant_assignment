# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core.validators import MaxValueValidator
from django.core.validators import MinValueValidator
# Create your models here.

class Restaurant(models.Model):
    restaurant_name = models.CharField(max_length=25)
    lattitude = models.FloatField(validators=[MaxValueValidator(90),MinValueValidator(-90)])
    longitude = models.FloatField(validators=[MaxValueValidator(180),MinValueValidator(-180)])
    city = models.CharField(max_length=15)
    discount_code = models.CharField(max_length=10)
    address = models.CharField(max_length=35)
    type = models.CharField(max_length=15)
    
    def __str__(self):
        return self.restaurant_name


class Menu(models.Model):
    dish_name = models.CharField(max_length=20)
    price = models.IntegerField(default=0)
    description = models.CharField(max_length=50)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)

    def __str__(self):
        return self.dish_name

class Discount(models.Model):
    discount_code = models.CharField(max_length=10)
    discount_tnc = models.CharField(max_length = 50)
    discount_percentage = models.IntegerField(validators=[MaxValueValidator(100),MinValueValidator(0)])
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)

    def __str__(self):
        return self.discount_code